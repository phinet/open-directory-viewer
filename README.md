# README

A tampermonkey script designed to show all files in an open directory

Usage:

- Show all links on current page: hold CTRL and press the 'a' key 3 times
- Show all links recursively: hold CTRL and press the 'a' key 4 times

Installation:

1. Have [Tampermonkey](https://www.tampermonkey.net/) or similar installed in your browser
2. Click here: [Install Script](https://bitbucket.org/phinet/open-directory-viewer/raw/master/open-directory-viewer.user.js) (Tampermonkey should prompt to install the script)