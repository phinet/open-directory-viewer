// ==UserScript==
// @name         Open Directory Viewer
// @namespace    https://bitbucket.org/phinet/open-directory-viewer
// @updateURL    https://bitbucket.org/phinet/open-directory-viewer/raw/master/open-directory-viewer.user.js
// @version      0.1
// @description  Explores Open Directories
// @author       Marcus Minhorst
// @match        *://*/*
// @grant        none
// ==/UserScript==

(function() {
    "use strict";

    let popupOuter, popupInner;

    let createPopup = (string) => {
        if (popupInner) {
            popupInner = null;
        }

        if (popupOuter) {
            popupOuter.remove();
            popupOuter = null;
        }

        let outer = document.createElement("div");

        outer.style.position = "fixed";
        outer.style.zIndex = "100";
        outer.style.top = "0";
        outer.style.left = "0";
        outer.style.width = "100%";
        outer.style.height = "100%";
        outer.style.background = "rgba(0,0,0,0.75)";
        outer.style.boxSizing = "border-box";
        outer.style.padding = "5em";
        outer.style.color = "black";
        outer.style.fontFamily = "monospace";
        outer.style.fontSize = "12pt";

        outer.addEventListener("click", () => {
            if (progress.length > 0) return;
            document.body.removeChild(outer);
            count = 0;
        });

        let inner = document.createElement("div");

        inner.style.width = "100%";
        inner.style.height = "100%";
        inner.style.overflow = "auto";
        inner.style.boxSizing = "border-box";
        inner.style.background = "white";
        inner.style.whiteSpace = "pre";
        inner.style.padding = "1em";

        inner.setAttribute("contenteditable", true);

        inner.addEventListener("click", (e) => e.stopPropagation(), true);

        outer.appendChild(inner);

        popupOuter = outer;
        popupInner = inner;

        document.body.appendChild(outer);

        if (string && string.length > 0) {
            inner.appendChild(document.createTextNode(string));

            let range = document.createRange();

            range.selectNode(inner.childNodes[0]);

            let selection = window.getSelection();

            selection.removeAllRanges();
            selection.addRange(range);
        }
    }

    let progress = [];

    let printProgress = (url) => {
        if (popupInner) {
            popupInner.textContent = progress.join("\n") + "\n" + url;
        }
    }

    let indent = (string, indentation) => indentation + string.trim().split("\n").join("\n" + indentation) + "\n";

    let loadFrame = (callback, url) => {
        var frame = document.createElement("iframe");
        frame.src = url;
        frame.style.position = "absolute";
        frame.style.width = "1em";
        frame.style.height = "1em";
        frame.style.top = "0";
        frame.style.left = "0";
        frame.style.height = "1em";
        frame.style.visibility = "hidden";
        document.body.appendChild(frame);

        frame.onload = () => {
            getLinks(
                (e) => {
                    frame.remove();
                    callback(e);
                },
                frame.contentDocument.body,
                url,
                true
            );
        }
        frame.onerror = () => {
            frame.remove();
            callback("\n");
        }
    }

    let getLinks = (callback, source, source_url, recurse) => {
        let links = source.getElementsByTagName("a");

        links = Array.from(links);

        links = links.filter(link => link.href);
        links = links.map(link => link.href.split(/[?#]/)[0]);
        links = links.filter(link => link.indexOf(source_url) === 0 && link.length !== source_url.length);

        links = [...new Set(links)].sort();

        let string = "";

        let folders = links.filter(link => link.endsWith("/"));
        let files = links.filter(link => !link.endsWith("/"));

        if (files.length > 0) {
            files.forEach((link) => { string += link + "\n" } );
        }

        if (!recurse || folders.length === 0) {
            if (folders.length > 0) {
                folders.forEach((link) => { string += link + "\n" } );
            }

            callback(string);
            return;
        } else {
            let index = 0;

            let loop = () => {

                if (index >= folders.length) {
                    callback(string);
                    return;
                }

                string += folders[index] + "\n";

                progress.push(1 + index + "/" + folders.length);

                printProgress(folders[index]);

                loadFrame((contents) => {
                    progress.pop();
                    string += indent(contents, "  ");
                    index++;
                    loop();
                }, folders[index]);
            }

            loop();
        }
    }

    let getRootLinks = () => {
        createPopup()
        getLinks(createPopup, document.body, window.location.href, false);
    }

    let getDeepRootLinks = () => {
        createPopup()
        getLinks(createPopup, document.body, window.location.href, true);
    }

    let count = 0;

    window.addEventListener("keydown", (e) => {
        const ctrlA = e.ctrlKey && e.key === "a";

        if (count < -1) return;

        if (!ctrlA) {
            count = 0;
            return;
        }

        if (count === -1) {
            getDeepRootLinks();
            count = -2;
            return;
        }

        count++;

        if (count < 3) return;

        count = -1;

        return getRootLinks();
    });
})();
